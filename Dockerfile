FROM tiangolo/uwsgi-nginx:python3.8

# By default, the application runs on port 80, you can choose a different port by uncommenting the LISTEN PORT and EXPOSE
# ENV LISTEN_PORT 8000
# EXPOSE 8000

# RUN pip install flask

ENV STATIC_URL /static

ENV STATIC_PATH /app/static

# If STATIC_INDEX is 1, serve / with /static/index.html directly (or the static URL configured in this case it will be 0)
# ENV STATIC_INDEX 1
ENV STATIC_INDEX 0

COPY ./app /app

WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt

ENV PYTHONPATH=/app

RUN mv /entrypoint.sh /uwsgi-nginx-entrypoint.sh

COPY entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/start.sh"]