import psycopg2
import pandas as pd
import numpy as np
import sqlalchemy as db
import configparser

config = configparser.RawConfigParser()
config.read('config.cfg')
args = dict(config.items('connection parameters'))

db_config = psycopg2.connect(
    host=args['host'],
    database=args['database'],
    user=args['user'],
    password=args['password'],
    port=args['port']
)

connection_db = db_config.cursor()


def get_all_members():
    try:
        connection_db.execute(f'SELECT id FROM public."user" where verified_public_key=true;')
        results = [r[0] for r in connection_db.fetchall()]
        return results
    except Exception:
        return []
