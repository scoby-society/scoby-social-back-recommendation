# db imports
import psycopg2
import configparser

config = configparser.RawConfigParser()
config.read('config.cfg')
args = dict(config.items('connection parameters'))

db_config = psycopg2.connect(
    host=args['host'],
    database=args['database'],
    user=args['user'],
    password=args['password'],
    port=args['port']
)

connection_db =  db_config.cursor()
