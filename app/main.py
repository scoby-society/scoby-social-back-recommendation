from db.users import get_all_members
from flask import Flask, jsonify, request
from rank_search.ranking_search import ranking_search, ranking_members
from rank_search.ranking_by_royalties import rank_by_royalties

app = Flask(__name__)


@app.route('/rank_user/<user_id>', methods=['GET'])
def rank_user(user_id):
    members = get_all_members()
    list_ranking = ranking_members(
        members=members, 
        source_user=int(user_id) , 
        is_internal=False
    )
    return jsonify([list_ranking])


@app.route('/rank_royalties', methods=['GET'])
def rank_royalties():
    response = []
    members = get_all_members()
    list_ranking = rank_by_royalties(members, None)
    if len(list_ranking) != 0:
        response = [key for key, value in list_ranking.items()]
    return jsonify([response])


@app.route('/rank_search', methods=['POST'])
def rank_search():
    response = ranking_search(request.json)
    return jsonify(response)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000,
        debug=True
    )
