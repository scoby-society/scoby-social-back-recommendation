from .utils import order_by_response
from db.connect import connection_db


def get_collections_user(user_id):
    try:
        connection_db.execute(
            f"SELECT symbol FROM public.spore_ds where user_id={user_id}"
        )
        results = [r for r in connection_db.fetchall()]
        results = [item[0] for item in results]
        results = set(results)
        results = [item for item in results if item != None]
        return list(results)
    except Exception:
        return []


def rank_by_collections(members, source_user):
    collections_source = get_collections_user(user_id=source_user)
    rank_collections = {}
    common_collections = []
    if len(collections_source) != 0:
        for user_id in members:
            # get topics user
            collections_members = get_collections_user(user_id=user_id)
            for collection_member in collections_source:
                # compare topics
                for collection_member in collections_members:
                    if collection_member == collection_member:
                        common_collections.append(collection_member)

                rank_collections[user_id] = len(common_collections)
                common_collections = []

        results_finish = order_by_response(rank_collections)
        return results_finish
    else:
        dict_members = {}
        for item in members:
            dict_members[item] = 0
        return dict_members

# get_pool_info


def get_users_collections(collection_id):
    try:
        connection_db.execute(
            f"SELECT symbol FROM public.pool where id={collection_id};")
        response_db = connection_db.fetchone()[0]
        connection_db.execute(
            f"SELECT user_id FROM public.spore_ds where symbol='{response_db}';")
        response_db = [r[0] for r in connection_db.fetchall() if r[0] != None]
        response_db = set(response_db)
        return list(response_db)
    except Exception:
        return []


def ranking_collections(
    source_user,
    collections,
    callback
):
    rank_collection = {}
    for collection in collections:
        members = get_users_collections(collection)
        if len(members) != 0:
            members = callback(
                source_user=source_user,
                members=members,
                is_internal=True
            )
            rank_collection[collection] = len(members)
        else:
            rank_collection[collection] = 0

    results_finish = order_by_response(rank_collection)
    results_finish = [r for r in results_finish.keys()]
    return results_finish
