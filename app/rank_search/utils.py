def partition(l, r, nums):
    # Last element will be the pivot and the first element the pointer
    pivot, ptr = nums[r], l
    for i in range(l, r):
        if nums[i] <= pivot:
            # Swapping values smaller than the pivot to the front
            nums[i], nums[ptr] = nums[ptr], nums[i]
            ptr += 1
    # Finally swapping the last element with the pointer indexed number
    nums[ptr], nums[r] = nums[r], nums[ptr]
    return ptr

def quicksort(l, r, nums):
    if len(nums) == 1:  # Terminating Condition for recursion. VERY IMPORTANT!
        return nums
    if l < r:
        pi = partition(l, r, nums)
        quicksort(l, pi-1, nums)  # Recursively sorting the left values
        quicksort(pi+1, r, nums)  # Recursively sorting the right values
    return nums

def list_duplicate_members(listNums):
    once = set()
    seenOnce = once.add
    twice = set(num for num in listNums if num in once or seenOnce(num))
    return list(twice)

def order_by_response(ranking_objects):
    results_finish = {}
    ranking_objects_values = [i for i in ranking_objects.values()]
    reference_objects = quicksort(0, len(ranking_objects_values)-1, ranking_objects_values)
    if len(reference_objects) != 0:
        set_topics = set(reference_objects)
        reference_objects = list(set_topics)
        reference_objects.reverse()
        for order in reference_objects:
            for key, value in ranking_objects.items():
                if value == order:
                    results_finish[key] = value
    return results_finish