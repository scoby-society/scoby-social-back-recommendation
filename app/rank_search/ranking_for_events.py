from .utils import order_by_response
from db.connect import connection_db


def get_users_events(event_id):
    try:
        connection_db.execute(
            f"SELECT user_id FROM public.events_subscribed_users where event_id={event_id};")
        response_db = [r[0] for r in connection_db.fetchall() if r[0] != None]
        response_db = set(response_db)
        return list(response_db)
    except Exception as e:
        return []


def ranking_events(
    source_user,
    events,
    callback
):
    rank_events = {}
    for team in events:
        members = get_users_events(team)
        if len(members) != 0:
            members = callback(
                source_user=source_user,
                members=members,
                is_internal=True
            )
            rank_events[team] = len(members)
        else:
            rank_events[team] = 0

    results_finish = order_by_response(rank_events)
    results_finish = [r for r in results_finish.keys()]
    return results_finish
