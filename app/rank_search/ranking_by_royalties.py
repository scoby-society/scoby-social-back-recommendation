from .utils import order_by_response
from db.connect import connection_db


def get_royalties_db(user_id):
    try:
        connection_db.execute(
            f"SELECT total_royalties FROM public.social_network where user_id={user_id}"
        )
        results = [r[0] for r in connection_db.fetchall()]

        if results is None or len(results) == 0:
            return 0
        else:
            return results[0]
    except Exception:
        return []


def rank_by_royalties(members, source_user):
    rank_royalties = {}
    for user_id in members:
        royalties_user = get_royalties_db(user_id=user_id)
        rank_royalties[user_id] = royalties_user
    results_finish = order_by_response(rank_royalties)
    return results_finish
