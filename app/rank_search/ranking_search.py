from .members import ranking_members
from .ranking_collections import ranking_collections
from .raking_by_teams import ranking_communities
from .ranking_for_events import ranking_events
from .raking_series import ranking_series

def ranking_search(request_json):
    response_results = []
    try:
        source_user = request_json.get('source_user', None)
        members = request_json.get('members', None)
        collections = request_json.get('collections', None)
        communities = request_json.get('communities', None)
        events = request_json.get('events', None)
        experiencies = request_json.get('experiencies', None)
        projects = request_json.get('projects', None)
    except Exception as e:
        return {
            "message": e.args[0]
        }

    # Raking for Members
    if members:
        if len(members) != 0:
            members = ranking_members(
                members=members,
                source_user=source_user,
                is_internal=False
            )
            response_results.append({"members": members})

    # Ranking For collections
    if len(collections) != 0:
        collections = ranking_collections(
            source_user=source_user,
            collections=collections,
            callback=ranking_members
        )
        response_results.append({"collections": collections})

    # Ranking For Communities
    if len(communities) != 0:
        communities = ranking_communities(
            source_user=source_user,
            commnunities=communities,
            callback=ranking_members
        )
        response_results.append({"communities": communities})

    # Ranking For events
    if len(events) != 0:
        events = ranking_events(
            source_user=source_user,
            events=events,
            callback=ranking_members
        )
        response_results.append({"events": events})

    # Ranking for Experiencies / experiencies == series
    if len(experiencies) != 0 :
        experiencies = ranking_series(
            source_user=source_user,
            experiencies=experiencies,
            callback=ranking_members
        )
        response_results.append({"experiencies": experiencies})


    return response_results
