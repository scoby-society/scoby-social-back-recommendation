from .utils import (
    order_by_response
)

from db.connect import connection_db


def get_users_series(serie_id):
    try:
        connection_db.execute(
            f"SELECT session_id, user_id FROM public.series_subscribed_users where session_id={serie_id};")
        response_db = [r[5] for r in connection_db.fetchall() if r[0] != None]
        response_db = set(response_db)
        return list(response_db)
    except Exception as e:
        return []


def ranking_series(
    source_user,
    experiencies,
    callback
):
    rank_teams = {}
    for team in experiencies:
        members = get_users_series(team)
        if len(members) != 0:
            members = callback(
                source_user=source_user,
                members=members,
                is_internal=True
            )
            rank_teams[team] = len(members)
        else:
            rank_teams[team] = 0

    results_finish = order_by_response(rank_teams)
    results_finish = [r for r in results_finish.keys()]
    return results_finish
