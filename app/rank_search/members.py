from .utils import list_duplicate_members, order_by_response
from .ranking_by_royalties import rank_by_royalties
from .ranking_collections import rank_by_collections

from db.connect import connection_db


def get_topics_user_db(user_id):
    try:
        connection_db.execute(
            f"SELECT topic_id FROM public.users_topics where user_id={user_id}"
        )
        results = [r[0] for r in connection_db.fetchall()]
        return results
    except Exception:
        return []


def rank_by_topics(members, source_user):
    topics_source = get_topics_user_db(source_user)
    rank_topic = {}
    common_topic = []
    if len(topics_source) != 0:
        for user_id in members:
            # get topics user
            topics_member = get_topics_user_db(user_id)
            for topic_source in topics_source:
                # compare topics
                for topic_member in topics_member:
                    if topic_source == topic_member:
                        common_topic.append(topic_member)

                rank_topic[user_id] = len(common_topic)
                common_topic = []
        results_finish = order_by_response(rank_topic)
        return results_finish
    else:
        dict_members = {}
        for item in members:
            dict_members[item] = 0
        return dict_members


def get_followers_db(user_id):
    try:
        connection_db.execute(
            f"select target_user_id from public.users_follow_users where source_user_id={user_id};"
        )
        results = [r[0] for r in connection_db.fetchall()]
        return results
    except Exception:
        return []


def rank_by_followers(members, source_user):
    followers_source = get_followers_db(source_user)
    rank_follower = {}
    common_follower = []
    if len(followers_source) != 0:
        for user_id in members:
            # get followers user
            followers_member = get_followers_db(user_id)
            for follower_source in followers_source:
                # compare followers
                for follower_member in followers_member:
                    if follower_source == follower_member:
                        common_follower.append(follower_member)

            rank_follower[user_id] = len(common_follower)
            common_follower = []

        results_finish = order_by_response(rank_follower)
        return results_finish
    else:
        dict_members = {}
        for item in members:
            dict_members[item] = 0
        return dict_members


def check_members_list(rank_members):
    members = [
        key
        for key, value in rank_members.items() if value == 0
    ]
    list_duplicate = list_duplicate_members(rank_members.values())
    members += [
        key
        for key, value in rank_members.items() if value in list_duplicate
    ]
    members = set(members)
    return list(members)


def ranking_members(members, source_user, is_internal):
    [members.remove(i) for i in members if i == source_user]
    rank_members = rank_by_followers(members, source_user)
    results_rank = {}

    if len(rank_members) != 0:
        members = check_members_list(rank_members=rank_members)
        if len(members) != 0:
            [
                rank_members.pop(item)
                for item in members
            ]
            topic_results = rank_by_topics(members, source_user)

            if len(topic_results) != 0:
                members = check_members_list(rank_members=topic_results)
                if len(members) != 0:
                    collections_results = rank_by_collections(
                        members=members,
                        source_user=source_user
                    )

                    if len(collections_results) != 0:
                        members = check_members_list(
                            rank_members=collections_results
                        )
                        if len(members) != 0:
                            royalties_results = rank_by_royalties(
                                members=members,
                                source_user=source_user
                            )

                            members = check_members_list(
                                rank_members=royalties_results
                            )

                            if is_internal:
                                if len(members) != 0:
                                    for key in members:
                                        royalties_results.pop(key)

                            results_rank = royalties_results

                        else:  # collection results not 0 or repeat
                            results_rank = collections_results
                else:  # topic members not 0 or repeat
                    results_rank = topic_results

        results_rank = [key for key, value in results_rank.items()]
        rank_members = [key for key, value in rank_members.items()]
        for result_rank in results_rank:
            rank_members.append(result_rank)

        return rank_members
