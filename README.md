# Scoby Social Back Recommendation

The application can be launched as a process directly on the host machine or in a Docker container.

- [Scoby Social: Recommendation API](#scoby-social-recommendation-api)
- [Process directly on the host machine](#process-directly-on-the-host-machine)
  - [Requirement](#requirement)
  - [Steps](#steps)
  - [Test](#test)
- [Docker container](#docker-container)
  - [Requirements](#requirements)
  - [Steps](#steps-1)
  - [Test](#test-1)

# Process directly on the host machine

## Requirement
- Python = 3.8

## Steps
```
cd app
pip install --no-cache-dir -r requirements.txt
python3 main.py
```

## Test
```
curl localhost/rank_user
```

# Docker container

## Requirements
 - Docker

## Steps
```
docker build -t scoby-social/recommendation-api .
docker run -d -p 80:80 scoby-social/recommendation-api
```

## Test
```
curl localhost/rank_user
```
